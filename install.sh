#!/bin/bash

#
# Проверить на существование программы
# 

requirementPrograms="fasm g++ gcc roxxiso grub qemu-kvm qemu"

CheckProg() {
    isError=false
    for progName in $@
    do
        if ! which $progName 1>/dev/null
        then
            isError=true
            echo "Not found: $progName. Installing.."
            apt-get install $progName -y
	else
            echo "Founded: $progName"
    fi
    done
}

CheckProg $requirementPrograms
