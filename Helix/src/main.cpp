//////////////////////////////////////////////////////////////////////////
//
//				       *** Helix ***
//					Copyright (C) 2018
//
// Обратная связь:		    https://vk.com/zombihello
// Репозиторий ОС:          https://gitlab.com/zombihello/helix
// Авторы данного файла:    zombiHello (Егор Погуляка)
//
//////////////////////////////////////////////////////////////////////////

#include "libc/stdio.h"

extern "C" void kernel_main()
{
	terminal_initialize( VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK );
	printf( "> Hello World!" );
	
	return;
}
