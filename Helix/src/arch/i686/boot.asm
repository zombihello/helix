format ELF

public start
extrn kernel_main

section ".text" executable
	align 4
	dd 0x1BADB002
	dd 0x00
	dd - ( 0x1BADB002 + 0x00 )

start:
	call kernel_main
	cli
	hlt

