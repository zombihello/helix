//////////////////////////////////////////////////////////////////////////
//
//				       *** Helix ***
//					Copyright (C) 2018
//
// Обратная связь:		    https://vk.com/zombihello
// Репозиторий ОС:          https://gitlab.com/zombihello/helix
// Авторы данного файла:    zombiHello (Егор Погуляка)
//
//////////////////////////////////////////////////////////////////////////

#include "tty.h"
#include "vga.h"

//-------------------------------------------------------------------//

#define VGA_WIDTH           80
#define VGA_HEIGHT          25
#define VGA_MEMORY          ( uint16_t* ) 0xB8000

//-------------------------------------------------------------------//

static size_t               terminal_row;
static size_t               terminal_column;
static uint8_t              terminal_color;
static uint16_t*            terminal_buffer;

//-------------------------------------------------------------------//

/* 
 * Инициализировать терминал
 */

void terminal_initialize( VGA_COLOR textColor, VGA_COLOR backgroundColor )
{
    terminal_row = 0;
    terminal_column = 0;
    terminal_color = vga_entry_color( textColor, backgroundColor );
    terminal_buffer = VGA_MEMORY;

    for ( size_t y = 0; y < VGA_HEIGHT; ++y )
        for ( size_t x = 0; x < VGA_WIDTH; ++x )
        {
            const size_t index = y * VGA_WIDTH + x;
            terminal_buffer[ index ] = vga_entry( ' ', terminal_color );
        }
}

//-------------------------------------------------------------------//

/*
 * Очистить терминал
 */

void terminal_clear()
{
    terminal_row = 0;
    terminal_column = 0;

    for ( size_t y = 0; y < VGA_HEIGHT; ++y )
        for ( size_t x = 0; x < VGA_WIDTH; ++x )
        {
            const size_t index = y * VGA_WIDTH + x;
            terminal_buffer[ index ] = vga_entry( ' ', terminal_color );
        }
}

//-------------------------------------------------------------------//

/*
 * Задать цвет терминала
 */

void terminal_setcolor( uint8_t color )
{
    terminal_color = color;
}

//-------------------------------------------------------------------//

/* 
 * Напичатать символ по координатам
 */

void terminal_putentryat( unsigned char _char, uint8_t color, size_t x, size_t y )
{
    const size_t index = y * VGA_WIDTH + x;
    terminal_buffer[ index ] = vga_entry( _char, color );
}

//-------------------------------------------------------------------//

/*
 * Напичатать символ
 */

void terminal_putchar( char _char )
{
    terminal_putentryat( _char, terminal_color, terminal_column, terminal_row );

    if ( ++terminal_column == VGA_WIDTH )
    {
        terminal_column = 0;

        if ( ++terminal_row == VGA_HEIGHT )
            terminal_row = 0;
    }
}

//-------------------------------------------------------------------//

/*
 * Напичатать строку
 */

void terminal_write( const char* string )
{
    for ( size_t index = 0; string[ index ] != '\0'; ++index )
        terminal_putchar( string[ index ] );
}

//-------------------------------------------------------------------//