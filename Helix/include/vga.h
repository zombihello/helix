//////////////////////////////////////////////////////////////////////////
//
//				       *** Helix ***
//					Copyright (C) 2018
//
// Обратная связь:		    https://vk.com/zombihello
// Репозиторий ОС:          https://gitlab.com/zombihello/helix
// Авторы данного файла:    zombiHello (Егор Погуляка)
//
//////////////////////////////////////////////////////////////////////////

#ifndef VGA_H
#define VGA_H

//-------------------------------------------------------------------//

#include "libc/stdint.h"

//-------------------------------------------------------------------//

enum VGA_COLOR 
{
	VGA_COLOR_BLACK         = 0,
	VGA_COLOR_BLUE          = 1,
	VGA_COLOR_GREEN         = 2,
	VGA_COLOR_CYAN          = 3,
	VGA_COLOR_RED           = 4,
	VGA_COLOR_MAGENTA       = 5,
	VGA_COLOR_BROWN         = 6,
	VGA_COLOR_LIGHT_GREY    = 7,
	VGA_COLOR_DARK_GREY     = 8,
	VGA_COLOR_LIGHT_BLUE    = 9,
	VGA_COLOR_LIGHT_GREEN   = 10,
	VGA_COLOR_LIGHT_CYAN    = 11,
	VGA_COLOR_LIGHT_RED     = 12,
	VGA_COLOR_LIGHT_MAGENTA = 13,
	VGA_COLOR_LIGHT_BROWN   = 14,
	VGA_COLOR_WHITE         = 15,
};
 
//-------------------------------------------------------------------//

//////////////////////////////////////////////////////////////////////
/// \brief Получить цвет
///
/// \param[in] textColor Цвет текста
/// \param[in] backgroundColor Цвет фона
/// \return Выходной цвет (цвет текста + цвет фона)
//////////////////////////////////////////////////////////////////////
static inline uint8_t vga_entry_color( VGA_COLOR textColor, VGA_COLOR backgroundColor )
{ 
    return textColor | backgroundColor << 4;
}
 
//////////////////////////////////////////////////////////////////////
/// \brief Получить выходной символ с цветом
///
/// \param[in] _char Символ
/// \param[in] color Цвет
/// \return Выходной символ с цветом
//////////////////////////////////////////////////////////////////////
static inline uint16_t vga_entry( unsigned char _char, uint8_t color ) 
{
	return ( uint16_t ) _char | ( uint16_t ) color << 8;
}

#endif // VGA_H